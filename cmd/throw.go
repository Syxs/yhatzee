package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"yahtzee/sdk"

	"github.com/spf13/cobra"
)

// throwCmd represents the throw command
var throwCmd = &cobra.Command{
	Use:   "throw",
	Short: "Simulates a dice for a Yahtzee game",
	Run: func(cmd *cobra.Command, args []string) {
		results := make([]int, 5)
		var err error

		for i := 1; i < 4; i++ {
			var throwCount int
			for _, result := range results {
				if result == 0 {
					throwCount++
				}
			}

			fmt.Println("Throwing dice")

			diceValues := sdk.ThrowDice(throwCount)
			results = setResults(results, diceValues)
			printResults(results)

			if i == 3 {
				break
			}

			results, err = deleteResults(results)
			if err != nil {
				log.Fatalf("Failed to throw dice: %v", err)
				return
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(throwCmd)
}

func setResults(results, diceValues []int) []int {
	for i, result := range results {
		if result == 0 {
			results[i] = diceValues[0]
			diceValues = append(diceValues[:0], diceValues[1:]...)
		}
		if len(diceValues) == 0 {
			return results
		}
	}
	return results
}

func printResults(results []int) {
	fmt.Println("Dice results")
	for i, diceValue := range results {
		fmt.Printf("[%d]: %d\n", i+1, diceValue)
	}
}

func deleteResults(results []int) ([]int, error) {
	if len(results) != 5 {
		return nil, errors.New("incorrect result count")
	}

	var deleteCount int
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Input the dice to forget (1-5) or skip if you wish to not reroll any more dice.")
	for scanner.Scan() {

		input := scanner.Text()
		if input == "skip" {
			break
		}

		i, err := strconv.Atoi(input)
		if err != nil {
			return nil, err
		}

		if i < 1 || i > 5 {
			fmt.Printf("Incorrect input %d.\n", i)
			continue
		}

		if results[i-1] != 0 {
			results[i-1] = 0
		} else {
			fmt.Printf("Incorrect input %d, can not forget the same value twice.\n", i)
			continue
		}

		deleteCount++
		if deleteCount == 3 {
			break
		}
		fmt.Println("Input value for next dice to forget.")
	}
	if scanner.Err() != nil {
		return nil, scanner.Err()
	}
	return results, nil
}
