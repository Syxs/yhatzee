package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var help bool

var rootCmd = &cobra.Command{
	Use: "yatzhee",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("got here")
		if err := cmd.Usage(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	},
}

//Execute runs the root command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
