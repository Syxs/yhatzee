package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var rulesCmd = &cobra.Command{
	Use:   "rules",
	Short: "Prints the rules for yhatzee",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Object of the game")
		fmt.Println("The object of Yahtzee is to obtain the highest score from throwing 5 dice.")
		fmt.Println("The game consists of 13 rounds. In each round, you roll the dice and then score the roll in one of 13 categories.")
		fmt.Println("You must score once in each category")
		fmt.Println("The score is determined by a different rule for each category.")
		fmt.Println("The game ends once all 13 categories have been scored.")
		fmt.Println()
		fmt.Println("For full rules and additional information")
		fmt.Println("http://www.yahtzee.org.uk/rules.html")
	},
}

func init() {
	rootCmd.AddCommand(rulesCmd)
}
