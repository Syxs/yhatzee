package main

import (
	"yahtzee/cmd"
)

func main() {
	cmd.Execute()
}
