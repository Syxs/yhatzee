package sdk

import (
	"math/rand"
	"time"
)

// ThrowDice simulates as many d6 dice throws as given in count
func ThrowDice(count int) []int {
	results := make([]int, 0, count)
	rand.Seed(time.Now().UnixNano())
	for i := 1; i <= count; i++ {
		results = append(results, rand.Intn(6)+1)
	}
	return results
}
